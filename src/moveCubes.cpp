//============================================================================
// Name        : moveCubes.cpp
// Author      : Daniel V
//============================================================================

	#include <moveCubes.hpp>


	moveCubes::moveCubes(): connection_ip("127.0.0.1")
	, connection_port(19997) 
	, joint_names({"arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5", "gripper_finger_joint_l", "gripper_finger_joint_r"})
	, offset({d2r(-169), d2r(-65), d2r(147), d2r(-102.5), d2r(-167.5)})
	, origin_positions({KDL::Vector(-0.100, 0.275, 0.010), KDL::Vector(-0.025, 0.275, 0.010), KDL::Vector( 0.050, 0.275, 0.010)})
	, jointInterface(connection_ip, connection_port, joint_names)
	, index({0, 1, 2, 3, 4, 5, 6})
	{
		//initiliaze chain
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.0, M_PI, 0.147, 0.0)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.033, +M_PI_2, 0.0, offset[0] + M_PI)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.155, 0.0, 0.0, offset[1] - M_PI_2)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.135, 0.0, 0.0, offset[2])));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.0, M_PI_2, 0.0, offset[3] - M_PI_2)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.0, 0, 0.0, offset[4])));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.00, 0, -0.208, 0.0)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.00, M_PI, 0.0, 0.0)));

		//set joint limits based on documentation
		limits_max.resize(chain.getNrOfJoints());
		limits_min.resize(chain.getNrOfJoints());
		
		//set lower limits
		limits_min(0) = d2r(1) ;
		limits_min(1) = d2r(1);
		limits_min(2) = d2r(-287); 
		limits_min(3) = d2r(1);
		limits_min(4) = d2r(1);
		
		//set upper limits
		limits_max(0) = d2r(330);
		limits_max(1) = d2r(149);
		limits_max(2) = d2r(-1);
		limits_max(3) = d2r(190);
		limits_max(4) = d2r(323);
		
		//initialize the solver
		solver = new KinematicSolver(chain, limits_max, limits_min);
	}
	
	moveCubes::~moveCubes()
	{
		delete solver;
	}
	
	void moveCubes::move_objects()
	{
		KDL::JntArray q_out(chain.getNrOfJoints());
		int vec_size = origin_positions.size();
		KDL::Vector p;
		
		//Set orientation of the cubes with respect to the base
		KDL::Rotation r;
		r.DoRotZ(d2r(90));
		r.DoRotY(d2r(-180));
		
		for (int x = 0 ; x < vec_size; x++)
		{
			
			p = origin_positions[x];
			KDL::Frame obj_frame(r, p);
			
			//set destination frame
			p.y(-p.y());
			p.z(p.z() + 0.010);
			KDL::Frame dest_frame(r, p);
			
			//open gripper
			jointInterface.openGripper();
			
			//move to opject
			solver->get_joint_angles(q_out, obj_frame);
			send_joint_angles_lift(q_out);
			
			//close gripper
			jointInterface.closeGripper();
			sleep(5);
			
			// lift arm
			lift_object(q_out);
			
			//move to target position
			solver->get_joint_angles(q_out, dest_frame);
			send_joint_angles_put(q_out);
			
			//open gripper
			jointInterface.openGripper();
			sleep(5);
			
			// lift arms
			lift_object(q_out);
			
		}
		
	}
	
    
    double moveCubes::d2r(double v) 
    {
		return v / 180 * M_PI;
	}

	double moveCubes::r2d(double v)
	{
		return v * 180 / M_PI;
	}
	
	void moveCubes::send_joint_angles_lift(KDL::JntArray& joints)
	{
		std::cout << "Moving to lift an object" << std::endl;
		for (int i = 0;i < 4; i++) {
			jointInterface.setJointPosition(index[i], joints(i));
			sleep(1);
		}
    
	}

	void moveCubes::send_joint_angles_put(KDL::JntArray& joints)
	{
		std::cout << "Movig to object destination" << std::endl;
		for (int i = 0;i < 5; i++) {
			jointInterface.setJointPosition(index[i], joints(i));
			sleep(1);
		}
	}

	void moveCubes::lift_object(KDL::JntArray& joints)
	{
		jointInterface.setJointPosition(1, joints(1) - d2r(20));
		jointInterface.setJointPosition(3, joints(3) - d2r(30));
		sleep(2);
	}
	


   







