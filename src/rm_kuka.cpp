//============================================================================
// Name        : rm_kuka.cpp
// Author      : Daniel V
//============================================================================

#include <KukaYoubotArm.hpp>

int main(int argc, char **argv)
{
	KukaYoubotArm kuka_arm(argc, argv);
	kuka_arm.move_objects();
	return 0;
}
