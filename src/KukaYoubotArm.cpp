//============================================================================
// Name        : KukaYoubotArm.cpp
// Author      : Daniel V
//============================================================================

	#include <KukaYoubotArm.hpp>


	KukaYoubotArm::KukaYoubotArm(int argc, char **argv): connection_ip("127.0.0.1")
	, connection_port(19997) 
	, joint_names({"arm_joint_1", "arm_joint_2", "arm_joint_3", "arm_joint_4", "arm_joint_5", "gripper_finger_joint_l", "gripper_finger_joint_r"})
	, offset({d2r(-169), d2r(-65), d2r(147), d2r(-102.5), d2r(-167.5)})
	, origin_positions({KDL::Vector(-0.100, 0.20, 0.015), KDL::Vector(-0.025, 0.20, 0.015), KDL::Vector( 0.050, 0.20, 0.015)})
	, jointInterface(connection_ip, connection_port, joint_names)
	, index({0, 1, 2, 3, 4, 5, 6})
	, use_youbot(false)
	, youBotManipulator(NULL)
	, gripperSetPoint(NULL)
	, youbot_flag("-youbot")
	{
		//initiliaze chain
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.0, M_PI, 0.147, 0.0)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.033, +M_PI_2, 0.0, offset[0] + M_PI)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.155, 0.0, 0.0, offset[1] - M_PI_2)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.135, 0.0, 0.0, offset[2])));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.0, M_PI_2, 0.0, offset[3] - M_PI_2)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::RotZ),KDL::Frame::DH(0.0, 0, 0.0, offset[4])));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.00, 0, -0.208, 0.0)));
		chain.addSegment(KDL::Segment(KDL::Joint(KDL::Joint::None),KDL::Frame::DH(0.00, M_PI, 0.0, 0.0)));

		//set joint limits based on documentation
		limits_max.resize(chain.getNrOfJoints());
		limits_min.resize(chain.getNrOfJoints());
		
		//set lower limits
		limits_min(0) = d2r(1) ;
		limits_min(1) = d2r(1);
		limits_min(2) = d2r(-287); 
		limits_min(3) = d2r(1);
		limits_min(4) = 0.1107;
		
		//set upper limits
		limits_max(0) = d2r(330);
		limits_max(1) = d2r(149);
		limits_max(2) = d2r(-1);
		limits_max(3) = d2r(190);
		limits_max(4) = 5.63;
		

		//use youbot?
		if(argc > 1)
		{
			std::string arg(argv[1]);
			if (arg == youbot_flag)
			{
				//initialize youbot
				youBotManipulator = new youbot::YouBotManipulator("youbot-manipulator", "/opt/ros/hydro/share/youbot_driver/config");
				gripperSetPoint = new youbot::GripperBarSpacingSetPoint();
				youBotManipulator->doJointCommutation();
				youBotManipulator->calibrateManipulator();
				youBotManipulator->calibrateGripper();
				use_youbot = true;
			}
			else
			{
				std::cout << "Wrong parameter: youbot not initialized" << std::endl;
			}

		}

		//initialize the solver
		solver = new KinematicSolver(chain, limits_max, limits_min);

	}
	
	KukaYoubotArm::~KukaYoubotArm()
	{
		delete solver;
		delete youBotManipulator;
		delete gripperSetPoint;
	}
	
	void KukaYoubotArm::move_objects()
	{
		KDL::JntArray q_out(chain.getNrOfJoints());
		int vec_size = origin_positions.size();
		KDL::Vector p;
		
		//Set orientation of the grippper - Approximate the cube from top
		KDL::Rotation r;
		r.DoRotZ(d2r(90));
		r.DoRotY(d2r(-180));
		

		KDL::Vector v(0, 0, 0);
		double hight = 0.0;
		for (int x = 0 ; x < vec_size; x++)
		{
			
			//get the location of the current object
			p = origin_positions[x];
			KDL::Frame obj_frame(r, p);
			p.z(p.z() + 0.035);
			KDL::Frame obj_frame_t(r, p);

			KDL::Frame dest_frame;
			KDL::Frame dest_frame_t;


			//set destination frame
			//move_cubes_line(dest_frame, dest_frame_t, p, r);


			//stack cubes
			move_cubes_stack(dest_frame,  dest_frame_t, p, r, hight);

			//open gripper
			open_gripper();
			
			//move to top objcet
			solver->get_joint_angles(q_out, obj_frame_t);
			send_joint_angles_lift(q_out);

			//move to object
			solver->get_joint_angles(q_out, obj_frame);
			send_joint_angles_lift(q_out);

			//close gripper
			close_gripper();
			sleep(3);
			
			// lift arm
			//move to top objcet
			solver->get_joint_angles(q_out, obj_frame_t);
			send_joint_angles_lift(q_out);

			
			//move to target position
			solver->get_joint_angles(q_out, dest_frame);
			send_joint_angles_put(q_out);
			
			//open gripper
			open_gripper();
			sleep(3);
			
			// lift arm
			solver->get_joint_angles(q_out, dest_frame_t);
			send_joint_angles_put(q_out);
			//lift_object(q_out);

		}
		
	}
    
    double KukaYoubotArm::d2r(double v) 
    {
		return v / 180 * M_PI;
	}

	double KukaYoubotArm::r2d(double v)
	{
		return v * 180 / M_PI;
	}
	
	void KukaYoubotArm::send_joint_angles_lift(KDL::JntArray& joints)
	{
		std::cout << "Moving to lift an object" << std::endl;

		//orient gripper
		//jointInterface.setJointPosition(index[4], joints(4));
		//sleep(1);

		//move arm
		for (int i = 0;i < 5; i++) {
			jointInterface.setJointPosition(index[i], joints(i));
			if(use_youbot)
			{
				youBotManipulator->getArmJoint(i + 1).setData(joints(i) * radian);
			}

		}
		sleep(4);
    
	}

	void KukaYoubotArm::send_joint_angles_put(KDL::JntArray& joints)
	{
		std::cout << "Movig to object destination" << std::endl;
		for (int i = 0;i < 5; i++) {
			jointInterface.setJointPosition(index[i], joints(i));
			if(use_youbot)
			{
				youBotManipulator->getArmJoint(i + 1).setData(joints(i) * radian);
			}

		}
		sleep(4);
	}

	void KukaYoubotArm::lift_object(KDL::JntArray& joints)
	{
		double angle1 = joints(1) - d2r(20);
		double angle2 = joints(3) - d2r(20);

		jointInterface.setJointPosition(1, angle1);
		jointInterface.setJointPosition(3, angle2);

		if(use_youbot)
		{
			youBotManipulator->getArmJoint(2).setData(angle1 * radian);
			youBotManipulator->getArmJoint(4).setData(angle2 * radian);
		}

		sleep(2);
	}
	
	void KukaYoubotArm::open_gripper()
	{
		jointInterface.openGripper();
		if(use_youbot){
			gripperSetPoint->barSpacing = 0.0115 * meter;
			youBotManipulator->getArmGripper().setData(*gripperSetPoint);
		}
	}

	void KukaYoubotArm::close_gripper()
	{
		jointInterface.closeGripper();
		if(use_youbot){
			gripperSetPoint->barSpacing = 0.0001 * meter;
			youBotManipulator->getArmGripper().setData(*gripperSetPoint);
		}
	}


	void KukaYoubotArm::move_cubes_line(KDL::Frame& dest_frame, KDL::Frame& dest_frame_t, KDL::Vector& p, KDL::Rotation& r)
	{
		p.x(p.x());
		p.y(-p.y());
		p.z(p.z() - 0.033);
		KDL::Frame dest_frame_a(r, p); // goal position on the other side

		//lift the arm after placing the arm
		p.z(p.z() + 0.015);
		KDL::Frame dest_frame_t_a(r, p);

		dest_frame = dest_frame_a;
		dest_frame_t = dest_frame_t_a;

	}

	void KukaYoubotArm::move_cubes_stack(KDL::Frame& dest_frame, KDL::Frame& dest_frame_t, KDL::Vector& p, KDL::Rotation& r, double& height)
	{
		KDL::Vector dest_v(-0.025, -0.20, 0.015);
		dest_v.z(dest_v.z() + height);
		height += 0.023;

		KDL::Frame dest_frame_a(r, dest_v);

		dest_v.z(dest_v.z() + 0.015);
		KDL::Frame dest_frame_t_a(r, dest_v);

		dest_frame = dest_frame_a;
		dest_frame_t = dest_frame_t_a;

	}





