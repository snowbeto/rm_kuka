//============================================================================
// Name        : kinematicSolver.cpp
// Author      : Daniel V
//============================================================================

    #include <kinematicSolver.hpp>

	KinematicSolver::KinematicSolver(KDL::Chain &chain, KDL::JntArray& limits_max, KDL::JntArray& limits_min): 
	fk_solver(chain), 
	ik_solver_vel(chain), 
	limits_max(limits_max),
	limits_min(limits_min),
	ik_solver(chain, limits_min, limits_max, fk_solver, ik_solver_vel, 1000, 0.000001)
	{
	
	}
	
	KinematicSolver::~KinematicSolver()
	{
		
	}
	
	void KinematicSolver::get_joint_angles(KDL::JntArray& q_out,  KDL::Frame& dest_frame)
	{
		KDL::JntArray final_joints(q_out.rows());
		KDL::Frame result_frame;
		KDL::Vector v;
		KDL::JntArray q_in(q_out.rows());
	
		double dist = 10000000.0;
		double res = 0.0;
    
		int ress = 0;
		for (int i = 0; i < 100;i++)
		{
			//Generate the initial joint seed
			generate_random_guess(q_in);
			
			//get joint angles from inverse kinematic solver
			ress = ik_solver.CartToJnt(q_in, dest_frame, q_out);

			if(ress != 0)
			{
				continue;
			}
			
			//Compare the current sulution with the forward kinematic solver
			fk_solver.JntToCart(q_out, result_frame);
			
			//keep the closest solution
			res = get_distance(dest_frame.p, result_frame.p);
			if (res < dist)
			{
				dist = res;
				final_joints = q_out;
				v = result_frame.p;	
			}
		}
		
		q_out = final_joints;
		
	}
    
    double KinematicSolver::get_distance(KDL::Vector vec1, KDL::Vector vec2)
	{	
		double temp1 = pow(vec1.x() - vec2.x(), 2.0);
		double temp2 = pow(vec1.y() - vec2.y(), 2.0);
		double temp3 = pow(vec1.z() - vec2.z(), 2.0);
	
		return sqrt(temp1 + temp2 + temp3);
	}
	
	double KinematicSolver::get_random_angle(double min, double max)
	{
		unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
		std::uniform_real_distribution<double> unif(min, max);
		std::default_random_engine re(seed);
		return unif(re) ;
	}

	void KinematicSolver::generate_random_guess(KDL::JntArray& q_in)
	{
		for(int x = 0; x < 5; x++)
		{
			q_in(x) = get_random_angle(limits_min(x), limits_max(x));
		}
	}
