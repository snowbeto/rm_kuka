//============================================================================
// Name        : VRepJointInterface.h
// Author      : Matthias Füller, Daniel V
//============================================================================

#ifndef VREPJOINTINTERFACE_H_
#define VREPJOINTINTERFACE_H_

#include "JointInterface.h"


#include <vector>
#include <string>

class VRepJointInterface: public JointInterface {
private:

	int clientID;
	std::vector<int> handles;

public:
	VRepJointInterface(const std::string connection_ip, int connection_port, const std::vector<std::string> joint_names );
	virtual ~VRepJointInterface();

	void setJointPosition(int index, double pos);

	void setJointPosition(int index[], double positions[], int size);

	void setJointVelocity(int index, double vel);

	void setJointVelocity(int index[], double velocities[], int size);

	double getJointPosition(int index);

	double getJointVelocity(int index);
	
	void openGripper();
	
	void closeGripper();

	void getCubePosition(std::string name, std::vector<double>& rot);
};

#endif /* YOUBOT_H_ */
