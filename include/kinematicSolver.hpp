//============================================================================
// Name        : kinematicSolver.hpp
// Author      : Daniel V
//============================================================================

#ifndef KINEMATIC_SOLVER_HPP
#define KINEMATIC_SOLVER_HPP

#include <random>
#include <chrono>
#include <iostream>
#include <cmath>
#include <kdl/chain.hpp>
#include <kdl/chainfksolver.hpp>
#include <kdl/chainfksolverpos_recursive.hpp>
#include <kdl/chainiksolverpos_nr.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/frames.hpp>
#include <kdl/chainiksolverpos_lma.hpp>
#include <kdl/chainiksolverpos_nr_jl.hpp>
#include <kdl/chainiksolvervel_wdls.hpp>
#include <kdl/chainiksolvervel_pinv.hpp>
#include <kdl/chainiksolvervel_pinv_givens.hpp>
#include <kdl/chainiksolvervel_pinv_nso.hpp>

class KinematicSolver
{
	public:
	
	KinematicSolver(KDL::Chain &chain, KDL::JntArray& limits_max, KDL::JntArray& limits_min);
	
	~KinematicSolver();
	
	void get_joint_angles(KDL::JntArray& q_out,  KDL::Frame& dest_frame);
	
	private:
	KDL::ChainFkSolverPos_recursive fk_solver;
    KDL::ChainIkSolverVel_pinv ik_solver_vel;
    KDL::ChainIkSolverPos_NR_JL ik_solver;
    KDL::JntArray limits_max;
    KDL::JntArray limits_min;
    
    double get_distance(KDL::Vector vec1, KDL::Vector vec2);
	double get_random_angle(double min, double max);
	void generate_random_guess(KDL::JntArray& q_in);

	
	
};
#endif
