//============================================================================
// Name        : KukaYoubotArm.hpp
// Author      : Daniel V
//============================================================================
#ifndef MOVE_CUBES_HPP
#define MOVE_CUBES_HPP

#include <iostream>
#include <vector>
#include <string>
#include <extApi.h>
#include <vrep/VRepJointInterface.h>
#include <kinematicSolver.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/frames.hpp>
#include <youbot/YouBotManipulator.hpp> //http://janpaulus.github.io/da/d1b/usage.html
#include <youbot/YouBotGripper.hpp>

class KukaYoubotArm
{
	public:
	KukaYoubotArm(int argc, char **argv);
	~KukaYoubotArm();
	
	void move_objects();
	
	private:
	
	const std::string youbot_flag;
	const std::string connection_ip;
    const int connection_port;
    const std::vector<std::string> joint_names;
    const std::vector<double> offset;
    const std::vector<KDL::Vector> origin_positions;
    const std::vector<int> index;
    KDL::JntArray limits_min;
    KDL::JntArray limits_max;
    KDL::Chain chain;
    VRepJointInterface jointInterface;
    KinematicSolver* solver;
	youbot::YouBotManipulator* youBotManipulator;
	youbot::GripperBarSpacingSetPoint* gripperSetPoint;
	bool use_youbot;
    
    double d2r(double v);

	double r2d(double v);
	
	void send_joint_angles_lift(KDL::JntArray& joints);
	
	void open_gripper();

	void close_gripper();

	void send_joint_angles_put(KDL::JntArray& joints);

	void lift_object(KDL::JntArray& joints);
	
	void move_cubes_line(KDL::Frame& dest_frame, KDL::Frame& dest_frame_t, KDL::Vector& p , KDL::Rotation& r);
	void move_cubes_stack(KDL::Frame& dest_frame, KDL::Frame& dest_frame_t, KDL::Vector& p, KDL::Rotation& r, double& height);
};

#endif
