//============================================================================
// Name        : moveCubes.hpp
// Author      : Daniel V
//============================================================================
#ifndef MOVE_CUBES_HPP
#define MOVE_CUBES_HPP

#include <iostream>
#include <vector>
#include <string>
#include <extApi.h>
#include <vrep/VRepJointInterface.h>
#include <kinematicSolver.hpp>
#include <kdl/chain.hpp>
#include <kdl/frames_io.hpp>
#include <kdl/frames.hpp>



class moveCubes
{
	public:
	moveCubes();
	~moveCubes();
	
	void move_objects();
	
	private:
	
	const std::string connection_ip;
    const int connection_port;
    const std::vector<std::string> joint_names;
    const std::vector<double> offset;
    const std::vector<KDL::Vector> origin_positions;
    const std::vector<int> index;
    KDL::JntArray limits_min;
    KDL::JntArray limits_max;
    KDL::Chain chain;
    VRepJointInterface jointInterface;
    KinematicSolver* solver;
    
    double d2r(double v);

	double r2d(double v);
	
	void send_joint_angles_lift(KDL::JntArray& joints);
	
	void send_joint_angles_put(KDL::JntArray& joints);

	void lift_object(KDL::JntArray& joints);
	
	
};

#endif
