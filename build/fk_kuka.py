"""Daniel Vazquez
   RM SS 2015
   Youbot Forward Kinematic Solver
"""
import numpy as np

def get_T(dh_params):
   
    alpha_ = dh_params[0]
    a_ = dh_params[1]
    d_ = dh_params[2]
    theta_ = dh_params[3]
    
    alpha_ = np.radians(float(alpha_))
    sin_a = np.sin(alpha_)
    cos_a = np.cos(alpha_)
    
    sin_t = np.sin(theta_)
    cos_t = np.cos(theta_)
        
    T = np.array([
             [cos_t        , -sin_t       , 0     , a_],
             [sin_t * cos_a, cos_t * cos_a, -sin_a, -sin_a * d_],
             [sin_t * sin_a, cos_t * sin_a, cos_a , cos_a * d_],
             [0            , 0            , 0     , 1]
             ])
    
    return T
    


def get_FW(dh_parameters_matrix):
    
    num_rows = dh_parameters_matrix.shape[0]
    Ts = []
    Ts_i = []
    TF = np.identity(4)
    
    #get the transformation for each frame
    #compute intermidiate transpose
    #compute the final transformation
    for row in range(0, num_rows):
        Ts.append(get_T(dh_parameters_matrix[row]))
        TF = TF.dot(Ts[row])
        Ts_i.append(TF)
    
 
    return TF, Ts, Ts_i




def get_mapped_DH_matrix(thetas):
    #mapp the thehas (Pk's mapping)
    theta1 = thetas[0] + np.deg2rad(-169)
    theta2 = -thetas[1] + np.deg2rad(65) + (np.pi / 2)
    theta3 = -thetas[2] + np.deg2rad(-146)
    theta4 = -thetas[3] + (np.pi / 2) + np.deg2rad(102.5)
    theta5 = thetas[4] + np.deg2rad(165) 
        
    #Define a DH parameters matrix    
    dh_matrix = np.array(
                      [[0 ,  0    , 0.147, 0.0],
                      [180,  0    , 0    , theta1 ],
                      [90 , -0.033, 0    , theta2],
                      [0  , -0.155, 0    , theta3],
                      [0  , -0.135, 0    , theta4],
                      [90 ,  0    , 0    , theta5]
                     ])       
                 
    return dh_matrix


def get_final_position(thetas):
    #Thetas 
    #thetas = [2.93836, 2.020597, -1.88253, 3.36243, 3.01283]
    dh_matrix = get_mapped_DH_matrix(thetas)

    #Compute TF
    TF , Ts, Ts_i = get_FW(dh_matrix)
    
    #Print final transformation
    #print_matrix(final_position)
    final_position = [TF[0][3], TF[1][3], TF[2][3]]
    
    return final_position
